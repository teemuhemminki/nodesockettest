//Tarjoillaan verkkosivu käyttäjälle
var handler = function (req, res) {
    fs.readFile('./page.html', function (err, data) {
        if (err) {
            throw err;
        }
        res.writeHead(200);
        res.end(data);
    })
}

//Määritellään perusmuuttujia ja vaatimuksia
var app = require('http').createServer(handler);
var io = require('socket.io').listen(app);
var fs = require('fs');
var Moniker = require('moniker');
var port = 3000;

app.listen(port);

let users = [];
let number = Math.floor(Math.random() * 10);

//Luodaan socketin kuuntelija, joka tervehtii tulevaa käyttäjää, poistaa poistuvat käyttäjät ja käsittelee arvaukset
io.sockets.on('connection', function (socket) {
    var user = addUser();
    socket.emit('welcome', user);
    socket.on('disconnect', function () {
        removeUser(user);
        io.sockets.emit('response', { message: user.name + ' has left the game' });
    });
    //Otetaan arvaus kiinni, käsitellään se ja lähetetään response oliko oikein vai väärin
    socket.on('guess', function (data) {
        const guessedNumber = data.message;
        if (guessedNumber == number) {
            user.wins += 1;
            io.sockets.emit('response', { message: 'Winner winner chicken dinner, ' + user.name + ' guessed the right number ' + guessedNumber });
            number = Math.floor(Math.random() * 10);
        } else {
            io.sockets.emit('response', { message: 'User ' + user.name + ' guessed ' + guessedNumber + ' but it was wrong ' });
        }
    });
});

//Metodeja, joita käytetään käyttäjien hallintaan
const addUser = function () {
    var user = { //Luo uuden käyttäjän
        name: Moniker.choose(), //Hakee käyttäjälle satunnaisen nimen
        wins: 0
    }
    users.push(user); //Lisää käyttäjän taulukkoon
    updateUsers();
    return user;
}

//Etsii ja poistaa käyttäjän taulukosta
const removeUser = function (user) {
    for (let i = 0; i > users.length; i++) {
        if (user.name === users[i].name) {
            users.splice(i, 1);
            updateUsers();
            return;
        }
    }
}

//Tämän pohjalta voitaisiin rakentaa taulu joka näyttää kaikki tämänhetkiset käyttäjät
const updateUsers = function () {
    let str = '';
    for (let i = 0; i < users.length; i++) {
        let user = users[i];
        str += user.name + ' has won: ' + user.wins + ' times.';
    }
    io.sockets.emit('users', { users: str });
}